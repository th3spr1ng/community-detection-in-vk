def intersection(lst1, lst2): 
    lst3 = [value for value in lst1 if value in lst2] 
    return lst3

import vk_api

i = 1

token = '5edbabe31e34aa65f33b962ba60b9a2ab0bf97d0a8e0e58add0c635c0f7ba27e653ea93f71dfdf0ec276b'
group_id = 'phys_kek'

session = vk_api.VkApi(token=token)
vk = session.get_api()

everything = []

fields = ['sex', 'bdate', 'city', 'country']

max_val = 500 # always \leq to 20100
min_val = 0
ofs = max_val

offset = min_val

while True:
    result = vk.groups.getMembers(group_id=group_id, fields=fields, count=ofs, offset=offset)
    everything.append(result)
    if (ofs * i + min_val >= result['count']):
        break

    offset += ofs*i
    
    i += 1

#print(everything)

#print(result)
ids = []
for elements in everything:
    for j in elements['items']:
        if ('error' in j):
            continue

        if ('deactivated' not in j):
            if (j['is_closed'] == False or j['can_access_closed'] == True):
                ids.append(j['id'])

#print(*ids)

# in ids we store ids of all of the subscribers of community
f = open('workfile.txt', 'w')

connections = {}

for people in ids:
    friend_subscribers = []
    friends = []
    user_id = str(people)
    result = vk.friends.get(user_id=user_id,count=100,fields=fields) # Now there are 10 people
    
    if ('error' in result):
        continue

    for friend in result['items']: # check every friend for deactivation
        if ('deactivated' not in friend):
            if (friend['is_closed'] == False or friend['can_access_closed'] == True):
                friends.append(friend['id'])

    # in ids[] we have all ids; in frineds[] we have ffriends
    # lets intersect these lists

    friend_subscribers = intersection(ids,friends)
    
    # for friend in friends:
    #     friend_id = str(friend)
    #     request = vk.groups.isMember(user_id=friend_id,group_id=group_id)

    #     if (request):
    #         friend_subscribers.append(friend_id)
    #     #print(request)
    if (not friend_subscribers):
        continue
    s = [str(i) for i in friend_subscribers]
    res = (",".join(s))
    res =  '[' + res + ']'
    res = user_id + ':' + res + '\n'
    
    f.write(res)
    print(res)
    connections[user_id] = friend_subscribers
    #print(user_id, friend_subscribers)


print(connections)