import networkx as nx
import requests
import pandas
import numpy as np
import matplotlib.pyplot as plt


d = {}
f = open('properw.txt', 'r')

lines = f.readlines()

for line in lines:
    line.replace('[','').replace(']','').replace('\n','')
    linesplit = line.split(':')
    node = []
    linesplit[1].replace('[','').replace(']','').replace('\n','')
    direct = linesplit[1].split(',')
    for item in direct:
        node.append(item.replace('[','').replace(']','').replace('\n',''))
    tupnode = tuple(node)
    d[linesplit[0]] = tupnode

# print(*d)

# #d={'a': ('b', 'c', 'd'), 'b': ('a', 'd'), 'c' : 'a'} 
G = nx.Graph()
for fPers in d:
    for friend in d[fPers]:
        G.add_edge(fPers, friend)

Gc = max(nx.connected_component_subgraphs(G), key=len)

a = nx.convert.to_dict_of_lists(Gc)

nx.draw(Gc, with_labels=False, node_color='r', node_size=2,)

plt.show()

g = open('finalgraph.txt', 'w')

for key in a:
    s = [str(i) for i in a[key]]
    res = (",".join(s))
    res =  '[' + res + ']'
    res = key + ':' + res + '\n'
    g.write(res)