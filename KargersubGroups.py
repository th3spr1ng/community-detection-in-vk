import random
import copy
import networkx as nx
import requests
import pandas
import numpy as np
import matplotlib.pyplot as plt

d = {1: [2, 4], 2: [1, 3, 4, 5], 3 : [2, 5], 4 :[1, 2,7], 5 : [2, 3, 6], 6:[5, 7], 7:[6,4,8], 8:[7,9],9:[8,10],10:[9]}

# d = {}

# f = open('finalgraph.txt', 'r')
# lines = f.readlines()

# for line in lines:
#     line.replace('\n','').replace('[','').replace(']','')
#     linesplit = line.split(':')
    
#     node = []
    
#     linesplit[1].replace('[','').replace(']','').replace('\n','')
#     direct = linesplit[1].split(',')
    
#     for item in direct:
#         node.append(item.replace('[','').replace(']','').replace('\n',''))

#     d[linesplit[0]] = node
# # This is how we get our dictionary

g = open('subsets.txt', 'r')
d1 = {}
d2 = {}

lines = g.readlines()


flag = 0
for line in lines:
    if ('#' in line):
        flag = 1
        continue
    
    line.replace('\n','').replace('[','').replace(']','')
    linesplit = line.split(':')

    if (flag == 0):
        node = []
        
        linesplit[1].replace('[','').replace(']','').replace('\n','')
        direct = linesplit[1].split(',')
        
        for item in direct:
            node.append(item.replace('[','').replace(']','').replace('\n',''))

        d1[linesplit[0]] = node
    else:
        node = []
        linesplit[1].replace('[','').replace(']','').replace('\n','')
        direct = linesplit[1].split(',')
        
        for item in direct:
            node.append(item.replace('[','').replace(']','').replace('\n',''))

        d2[linesplit[0]] = node
# This is how we get our dictionary. d1 for first and d2 for second.

node_color = ['r','g','b']

def build_configuration(correspondance,initial_dict):
    print('Lol')
    leftset = []
    rightset = []
    k = 0
    some_length = 666

    for key in correspondance:
        if (k == 0):
            leftset = correspondance[key]
            k += 1
        else:
            rightset = correspondance[key]

    # print('leftset is: ', leftset)
    # print('rightset is: ', rightset)
    # leftset = list(correspondance.keys())[0]
    # rightset = list(correspondance.keys())[1]
    # Because we should have only 2 keys left at the end

    graph = {}
    for element in leftset:
        list_of_connected_nodes = initial_dict[element]
        graph[element] = [x for x in list_of_connected_nodes if (x not in rightset)]
    # We build the left part after division
    
    for element in rightset:
        list_of_connected_nodes = initial_dict[element]
        graph[element] = [x for x in list_of_connected_nodes if (x not in leftset)]

    # Build right part
    # print(graph)



    return graph
    # Graph has left and right parts, which are disjoint to each other


def delete_edge(v1,v2,dictionary,correspondance):
    m = [v1,v2]
    # random.shuffle(m)
    v1 = m[0]
    v2 = m[1]
    newV = v1 # maybe, it can be v2, but no difference I see
    ver1 = dictionary[v1]
    ver2 = dictionary[v2]
    # print(v1,v2)
    bigList = []
    for item in ver1:
        if (item != v1 and item != v2):
            bigList.append(item)

    for item in ver2:
        if (item != v1 and item != v2):
            bigList.append(item)

    #print(bigList)
    dictionary[newV] = bigList
    # New edge with all previous edges made now
    #print('OK')
    # Now have to connect other vertices to that vertice
    # How can we do that? Other connected vertices have v1 or v2
    # Not in their keys, but in their values.
    
    for item in ver1:
        a = [x for x in dictionary[item] if (x != v1 and x != v2)]
        dictionary[item] = a
                                    # now those connected to v1
                                    # are no more connected to it

    for item in ver2:
        a = [x for x in dictionary[item] if (x != v1 and x != v2)]
        dictionary[item] = a
                                    # now those connected to v2
                                    # are no more connected to it


    #print('dictionary after not including: ', dictionary)
    
    for item in ver1:
        a = []
        if (item != newV):
            for value in dictionary[item]:
                a.append(value)
            a.append(newV)
        else:
            for value in dictionary[item]:
                a.append(value)
        dictionary[item] = a

    for item in ver2:
        a = []
        if (item != newV):
            for value in dictionary[item]:
                a.append(value)
            a.append(newV)
        else:
            for value in dictionary[item]:
                a.append(value)
        dictionary[item] = a

    dictionary[newV] = bigList


    #print ('dictionary after including new node: ', dictionary)
    n1 = set(correspondance[v1])
    n2 = set(correspondance[v2])
    temp = n1.union(n2)
#    value = dictionary.pop(v1)
    value = dictionary.pop(v2)
    # There are no more vertices v1 and v2
    # Now have to connect vetices of v1 and v2 to newV
    
    # Now correspondance[newV] has the list of all consumed vertices

    value = correspondance.pop(v1)
    value = correspondance.pop(v2)
    correspondance[newV] = list(temp)
    # print('correspondance is: ',correspondance)
    # Now we cleared information about these vertices, as they are in
    # correspondance[newV]. Cleared in order to have 2 keys at the end.

def get_size(dictionary):
    i = 0
    for key in dictionary:
        i += 1
    return i

def Karger(dictionary):
    second_corr = {}
    for key in dictionary:
        l = []
        l.append(key)
        second_corr[key] = l

    # We shall use that list to remember which
    # Vertices are contained in which.
    # By default, they are self-contained
    # We shall have two such dictionaries
    # In order to remember the best option
    initial_dict = copy.deepcopy(dictionary) # So we remember first graph
    config = {}
    cut = 9999999999
    for j in range(0,2): # It should have been over 9000, but my maschine is too weak

    #    min_cut = 9999999999999 # Big number, no other are greater than that (I believe)
        dictionary = copy.deepcopy(initial_dict) # So we remember first graph
        correspondance = copy.deepcopy(second_corr)
        
        
        k = get_size(dictionary)
        
        for i in range(k - 2):
            # print(i,'iteration, dict: ', dictionary)
            vertices = []
            for key in dictionary:
                vertices.append(key)
            e = random.choice(vertices)
            vertices2 = []
            for elem in dictionary[e]:
                vertices2.append(elem)
            e2 = random.choice(vertices2)

            # Now we have some random edge

            delete_edge(e,e2,dictionary, correspondance)
            #print('new dict: (from karger): ',dictionary)
            m = get_size(dictionary)
            #print(m)
            if (m == 2 or m == 1):
                k = 0
                some_length = 777
                for key in dictionary:
                    if (k == 0):
                        some_length = len(dictionary[key])
                        # print(some_length)
                if (some_length < cut):
                    cut = some_length
                    # print('Cut is: ', cut)
                    config = build_configuration(correspondance, initial_dict)
                print(j,'some_length: ', some_length)

    print('Cut is: ', cut)

    d = {}
    for key in config:
        a = config[key]
        b = tuple(a)
        d[key] = b

    G = nx.Graph()
    for fPers in d:
        for friend in d[fPers]:
            G.add_edge(fPers, friend)

    return G

G1 = Karger(d1)
G2 = Karger(d2)
nx.draw(G1, with_labels=False, node_color='r', node_size=4)
nx.draw(G2, with_labels=False, node_color='b', node_size=4)
plt.show()