Community Detection in VK
TASK
For a given VK community, it is necessary to detect sub-communities inside, investigate them and
find dependencies.

Sequence of the files to run:

1) getid.py - from vk_api it writes graph into file workfile.txt

2) rewriteData.py - it cleans the data from workfile.txt and writes it
in properw.txt

3) createGraph.py - plot the graph and then make one connected component, 
and rewrite it in finalgraph.txt

4) Karger.py - plot two subgraphs which stand for sub-communities.

5) KargerGroups.py - plot two subsubgraphs, which represent dependencies in
the subgroups.

6) BestPartition.py - detect subcommunities using netwokx method best_partition,
which uses the Louvain method

Files 'Общий Отчёт' and 'Общий Отчет вторая версия' represent results of that project