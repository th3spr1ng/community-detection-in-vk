import community
import networkx as nx
import requests
import pandas
import numpy as np
import matplotlib.pyplot as plt

d = {}

f = open('finalgraph.txt', 'r')
lines = f.readlines()

for line in lines:
    line.replace('[','').replace(']','').replace('\n','')
    linesplit = line.split(':')
    node = []
    linesplit[1].replace('[','').replace(']','').replace('\n','')
    direct = linesplit[1].split(',')
    for item in direct:
        node.append(item.replace('[','').replace(']','').replace('\n',''))
    tupnode = tuple(node)
    d[linesplit[0]] = tupnode

G = nx.Graph()
for fPers in d:
    for friend in d[fPers]:
        G.add_edge(fPers, friend)


partition = community.best_partition(G)

size = float(len(set(partition.values())))
pos = nx.spring_layout(G)

count = 0.

for com in set(partition.values()) :
    count = count + 1.
    list_nodes = [nodes for nodes in partition.keys() if partition[nodes] == com]
    nx.draw_networkx_nodes(G, pos, list_nodes, node_size = 20, node_color = str(count / size))

nx.draw_networkx_edges(G,pos, alpha=0.5)
plt.show()